<?
if(!file_exists('libs/config.php')){
	header('Location: install.php'); //Устанока при первом запуске
}
//if(file_exists('install.php')) unlink('install.php');  //Удаление Install.php

//Подключаем библиотеки
require_once 'libs/config.php';
require_once 'libs/database_class.php';
require_once 'libs/files_upload_class.php';
require_once 'libs/format_class.php';

//Создаем переменные и объекты
$db = DataBase::getDB();
$msg = "";
$format = new Format();


//Загрузка файла, парсинг и добавление записей в базу
if (isset($_FILES['uploadfile']['type'])) {
	try{
		$f_upload = new Files_upload();
		$array_users = $f_upload->get_user_array();
		$msg = $db->upload_users($array_users);
	}
	catch (XMLException $e){
		$error = $e->getMessage();
	}
	catch (UserException $e){
		$error = $e->getMessage();
	}
	catch (Exception $e){
		$error = $e->getMessage();
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Test task</title>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="app">
	<div class="container">
		<div class="page-header">
		  <h1>Приложение</h1>
		</div>
		<p>В базе <span class="badge"><?=$db->get_records();?></span> запис<? echo $format->get_end($db->get_records());?></p>
		<div class="row">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<?if(isset($error)){?>
							<div class="alert alert-danger"><?=$error;?></div>
						<?}?>
						<h4>Файл для импорта в базу данных</h4>
						<div class="form-group">
							<form action="" method="post" enctype="multipart/form-data" >
							<div class="col-sm-6">
								<input type="file" name="uploadfile">
							</div>
							<div class="col-sm-6">
								<input type="submit" class="btn btn-success pull-right" value="Обновить базу данных"> </div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<?=$msg;?>
			</div>
		</div>

	</div>
	<script src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>