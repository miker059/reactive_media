<?
require_once 'config.php';
/**
* Mail class
*/
class Mail{
	private $config;
	
	function __construct(){
		$config = new Config();
	}

	//Оправка отчета на почту
	public function send($msg){
		$to = $this->config->email;
		$subject = "Отчет об обновлении";
		$headers = "Content-type: text/html; charset=UTF-8 \r\n";
		$headers .= "From: TEST\r\n";
		if(@mail($to, $subject, $msg, $headers)) return "Отчет выслан на почту";
		else return "Письмо с отчетом почему то не отправилось...";
	}
}