<?
require_once 'exception_class.php';
/**
* Files upload class
*/
class Files_upload{
	private $type;
	const CSV = 'application/vnd.ms-excel';
	const XML = 'text/xml';
	private $array = array();
	private $array_keys = array('login', 'password', 'name', 'email');

	//Получаем файл из $_FILES, определяем тип и передаем в обработку
	public function __construct(){
		$this->type = $_FILES['uploadfile']['type'];
		if($this->type == self::CSV) $this->getCSV();
		elseif($this->type == self::XML) $this->getXML();
		else throw new UserException("Некорректный файл");
		
	}

	private function getCSV(){
		$file = fopen($_FILES['uploadfile']['tmp_name'], "r"); 
		while (($array_user = fgetcsv($file, 1000, ";")) !== FALSE) {
			if(count($array_user) < 2 || count($array_user) == 3) 
				throw new UserException("Файл содержит недостаточно данных");
			if(count($array_user) == 2) {
				$array_user[] = $array_user[0];
				$array_user[] = $array_user[0].'@example.com';
			}
			array_push($this->array, array_combine($this->array_keys, $array_user));
		}
		fclose($file);
	}

	private function getXML(){
		$file = simplexml_load_file($_FILES['uploadfile']['tmp_name'], null, LIBXML_NOERROR);
		if(!is_object($file)){
			throw new XMLException(libxml_get_last_error());			
		}
		$result = $file->xpath("user");
		if(!count($result)){
			throw new UserException("Неверный формат файла. Не найден корневой элемент User.");
		}
		foreach ($result as $user) {
			$array_user = (array) $user;
			$login = $array_user['login'];
			$pasw = $array_user['password'];
			$name = isset($array_user['name'])?$array_user['name']:$array_user['login'];
			$email = isset($array_user['email'])?$array_user['email']:$array_user['login']."@example.com";
			array_push($this->array, array($this->array_keys[0] => $login, $this->array_keys[1] => $pasw, $this->array_keys[2] => $name, $this->array_keys[3]  => $email));
		}
	}

	//Передаем массив с данными из файла
	public function get_user_array(){
		return $this->array;
	}
}