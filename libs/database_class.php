<?
require_once 'config.php';
require_once 'mail_class.php';
require_once 'format_class.php';
/**
* Database class
*/
class Database{

	private static $db = null;
	private $config;
	private $mail;
	private $mysqli;
	private $format;
	private $count_upd = 0;
	private $count_del = 0;
	private $count_new = 0;

	//Исключаем множественные подключения
	public static function getDB(){
		if(self::$db == null) self::$db = new Database();
		return self::$db;
	}
	
	private function __construct(){
		$this->config = new Config();
		$this->mail = new Mail();
		$this->format = new Format();
		$this->mysqli = new mysqli($this->config->db_host, $this->config->db_user, $this->config->db_pass, $this->config->db_name);
			$this->mysqli->query("SET NAME 'utf8'");
	}

	private function result_set_to_array($result_set) {
		$array = array();
		while (($row = $result_set->fetch_assoc()) != false) {
			$array[] = $row;
		}
		return $array;
	}

	//Обновление БД
	public function upload_users($array){
		if(!is_array($array)) throw new Exception("Внутреняя ошибка. Попробуйте снова");
		$this->count_upd = $this->update_users($array);
		$this->count_new = $this->new_users($array);
		$this->count_del = $this->del_users($array);
		$msg = "Обновлено ".$this->count_upd." запис".$this->format->get_end($this->count_upd)." <br/>";
		$msg .= "Добавлено ".$this->count_new." новых запис".$this->format->get_end($this->count_new)." <br/>";
		$msg .= "Удалено ".$this->count_del." запис".$this->format->get_end($this->count_del)." <br/>";
		$msg .= $this->mail->send($msg);
		return $msg;
	}

	//Количество запсей в БД
	public function get_records(){
		$result = $this->mysqli->query("SELECT * FROM `".$this->config->db_name."`.`user`");
		return $result->num_rows;
	}

	private function update_users($array){
		$count_upd = 0;
		foreach ($array as $user) {
			$query = "UPDATE `".$this->config->db_name."`.`user` SET password='".$user['password']."', name='".$user['name']."', email='".$user['email']."' WHERE login = '".$user['login']."'";
			$this->mysqli->query($query);
			$count_upd = $count_upd + $this->mysqli->affected_rows;
		}
		return $count_upd;
	}

	private function new_users($array){
		$count_new = 0;
		foreach ($array as $user) {
			$query = "INSERT IGNORE INTO `".$this->config->db_name."`.`user` VALUES ('".$user['login']."','".$user['password']."','".$user['name']."','".$user['email']."')";
			$this->mysqli->query($query);
			$count_new = $count_new + $this->mysqli->affected_rows;
		}
		return $count_new;
	}

	private function del_users($array){
		$count_del = 0;
		$rec = $this->mysqli->query("SELECT * FROM `".$this->config->db_name."`.`user`");
		$array_base = $this->result_set_to_array($rec);
		foreach ($array_base as $key => $user_base) {
			foreach ($array as $user) {
				if($user_base['login'] == $user['login']){
					unset($array_base[$key]);
					break;
				}
			}			
		}
		foreach ($array_base as $user_base) {
			$query = "DELETE FROM `".$this->config->db_name."`.`user` WHERE login = '".$user_base['login']."'";
			$this->mysqli->query($query);
			$count_del = $count_del + $this->mysqli->affected_rows;
		}
		return $count_del;
	}

	public function __destruct() {
		if ($this->mysqli) $this->mysqli->close();
	}
}