<?
/**
* Formating class
*/
class Format{

	//Окончания для "записей"
	public function get_end($num){
		$number = substr($num, -2);
	    if($number > 10 and $number < 15){
	        $term = "ей";
	    }else{ 
	        $number = substr($number, -1);
	        if($number == 0) $term = "ей";
	        if($number == 1) $term = "ь";
	        if($number > 1) $term = "и";
	        if($number > 4) $term = "ей";
	    }
	    return $term;
	}
}