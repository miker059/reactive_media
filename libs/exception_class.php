<?

/**
* Exception class
*/

class XMLException extends Exception{
	private $error;

	public function __construct(LibXMLError $error){
		$shortfile = basename($error->file);
		$msg = "[{$shortfile}, строка {$error->line}, колонка {$error->column}] {$error->message}";
		$this->error = $error;
		parent::__construct($msg, $error->code);
	}

	public function get_lib_xml_error(){
		return $this->error;
	}
}

class UserException extends Exception{}