<? //Шаблон для создания config.php, файл создается автоматически при первом запуске
class Config{
	// configuration database 
	public $db_host 	= "#host#";
	public $db_name 	= "#name#";
	public $db_user 	= "#user#";
	public $db_pass 	= "#pasw#";
	public $db_charset 	= "utf8";

	//other properties
	public $email		= "#email#";
}