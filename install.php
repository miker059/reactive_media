<?
error_reporting(E_ERROR);

if(!isset($_REQUEST['step']) || $_REQUEST['step'] == 1) $step = 1;
else $step = $_REQUEST['step'];
$host = $_REQUEST['host'];
$name = $_REQUEST['name'];
$user = $_REQUEST['user'];
$pasw = $_REQUEST['pasw'];
$email = $_REQUEST['email'];

//Загрузка первоначального файла
function XMLload($db){
	$xml = simplexml_load_file($_FILES['uploadfile']['tmp_name']);
	if($xml){
		foreach ($xml->user as $user) {
			$name = $user->name?$user->name:$user->login;
			$email = $user->email?$user->email:$user->login;
			$query = "INSERT INTO `".$_REQUEST['name']."`.`user` VALUES ('".$user->login."','".$user->password."','".$name."','".$email."@example.com')";
			if(!$db->query($query)){
				printf($db->error);
			}
		}
		echo "Файл успешно загружен.<br/> В базу добавлено ".count($xml->user)." записей.";
	}else{
		echo "Неверный формат файла<br/>В базу небыло добавлено ни одной записи. <br/>Вы сможете добавить файл с записями позже.";
	}
	config_setup();
}

//Создаем конфиг
function config_setup(){
	global $host, $name, $user, $pasw, $email;
	echo "string";
	if(!file_exists('libs/config.php')){
		$conf_file = file('libs/config_temp.php');
		foreach ($conf_file as $key => $row) {
			if(strpos($row, '#host#')) $conf_file[$key] = str_replace("#host#", $host, $row);
			if(strpos($row, '#name#')) $conf_file[$key] = str_replace("#name#", $name, $row);
			if(strpos($row, '#user#')) $conf_file[$key] = str_replace("#user#", $user, $row);
			if(strpos($row, '#pasw#')) $conf_file[$key] = str_replace("#pasw#", $pasw, $row);
			if(strpos($row, '#email#')) $conf_file[$key] = str_replace("#email#", $email, $row);
		}
		foreach ($conf_file as $row) {
			file_put_contents('libs/config.php', $row, FILE_APPEND | LOCK_EX);
		}
	}
	echo "<br/>Установка успешно завершена!";
	echo '<meta http-equiv="Refresh" content="3; url=\'/\'">';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Install test task</title>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="install">
	<div class="container">
		<div class="page-header">
		  <h1>Начальные установки </h1>
		</div>
		<? //Шаг первый - собираем данные
		if($step == 1){?> 
		<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Настройки базы данных</h3>
			  </div>
			  <div class="panel-body">
			    	<div class="col-sm-6">
			    		<div class="panel panel-default">
			    			<div class="panel-body">
			    				<div class="group-checkbox">
		    						<input type="radio" id="db1" checked name="db" class="css-checkbox" value="1">
		    						<label for="db1" class="css-label">	
										Новая база данных
		    						</label>
		    					</div>
		    					<div class="group-checkbox">
		    						<input type="radio" id="db2" name="db" class="css-checkbox" value="2">
		    						<label for="db2" class="css-label">
		    							Существующая база данных
		    						</label>
		    					</div>
	    						<div class="form-group">
	    							<label class="col-sm-3 control-label">Хост </label>
	    							<div class="col-sm-9">
	    								<input class="form-control" name="host" type="text" value="localhost" required />
	    							</div>
	    						</div>
	    						<div class="form-group">
	    							<label class="col-sm-3 control-label">Имя БД </label>
	    							<div class="col-sm-9">
	    								<input class="form-control" name="name" type="text" value="users" required/>
	    							</div>
	    						</div>
	    						<div class="form-group">
	    							<label class="col-sm-3 control-label">Пользователь </label>
	    							<div class="col-sm-9">
	    								<input class="form-control" name="user" type="text" value="root" required/>
	    							</div>
	    						</div>
	    						<div class="form-group">
	    							<label class="col-sm-3 control-label">Пароль </label>
	    							<div class="col-sm-9">
	    								<input class="form-control" name="pasw" type="text"/>
	    							</div>
	    						</div>
			    			</div>
			    		</div>
			    	</div>
			    	<div class="col-sm-6">
			    		<div class="panel panel-default">
			    			<div class="panel-body">
			    				<h4>Файл для импорта в базу данных</h4>
			    				<div class="form-group">
			    					<div class="col-sm-12">
			    						<input type="file" name="uploadfile">
			    					</div>
			    				</div>
			    			</div>
			    		</div>
			    		<div class="panel panel-default">
			    			<div class="panel-body">
			    				<h4>Email для отчетов</h4>
			    				<div class="form-group">
			    					<div class="col-sm-12">
			    						<input type="email" name="email" class="form-control" placeholder="user@example.com" required>
			    					</div>
			    				</div>
			    			</div>
			    		</div>
			    		<div class="form-group">
			    			<div class="col-sm-12">
			    				<input type="hidden" name="step" value="2">
			    				<button class="btn btn-success pull-right" type="submit">Продолжить >></button>
			    			</div>
			    			
			    		</div>
			    	</div>
			    </div>
			</div>
		</form>
		<?// Шаг второй, все проверяем, добавляем базу и т.п.
		}elseif ($step == 2) {
			if($_REQUEST['db'] == 1){
				$db = new mysqli($host, $user, $pasw);
				if(mysqli_connect_errno()){
					echo "Ошибка связи с базой данных";
					echo '<meta http-equiv="Refresh" content="3; url=\'install.php\'">';
				}else{
					$db->query("SET NAME 'utf8'");
					$db->query("CREATE DATABASE `".$_REQUEST['name']."`");
					$db->query("CREATE TABLE IF NOT EXISTS `".$name."`.`user` (
						`login` varchar(255) NOT NULL PRIMARY KEY,
						`password` varchar(40) NOT NULL,
						`name` varchar(255) NOT NULL,
						`email` varchar(255) NOT NULL
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
					echo "База данных успешно создана.<br/>";
					XMLload($db);	
				}
			}else{
				$db = new mysqli($host, $user, $pasw, $name);
				if(mysqli_connect_errno()){
					echo "База данных не найдена или отсутствует связь с сервером. Проверте данные или создайте новую базу данных.<br/>";
					echo '<meta http-equiv="Refresh" content="3; url=\'install.php\'">';
				}else{
					$db->query("CREATE TABLE IF NOT EXISTS `".$name."`.`user` (
						`login` varchar(255) NOT NULL PRIMARY KEY,
						`password` varchar(40) NOT NULL,
						`name` varchar(255) NOT NULL,
						`email` varchar(255) NOT NULL
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
					echo "База успешно подключена.<br/>";
					XMLload($db);
				}
			}
			$db->close();
		}?>
		
	</div>
	
	<script src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>